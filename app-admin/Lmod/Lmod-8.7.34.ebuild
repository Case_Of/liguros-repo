# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit autotools lua-single prefix

DESCRIPTION="A Lua-based environment module system supporting TCL and software hierarchy"
HOMEPAGE="https://www.tacc.utexas.edu/tacc-projects/lmod"
SRC_URI="https://github.com/TACC/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~sparc ~x86"
IUSE="+auto-swap +cache duplicate-paths test"
REQUIRED_USE="${LUA_REQUIRED_USE}"
RESTRICT="!test? ( test )"

RDEPEND="${LUA_DEPS}
	dev-lang/tcl
	dev-lang/tk
	$(lua_gen_cond_dep '
		>=dev-lua/luafilesystem-1.8.0[${LUA_USEDEP}]
		dev-lua/luajson[${LUA_USEDEP}]
		dev-lua/luaposix[${LUA_USEDEP}]
		dev-lua/lua-term[${LUA_USEDEP}]
	')
	virtual/pkgconfig
"
DEPEND="${RDEPEND}"
BDEPEND="${RDEPEND}
	test? (
		$(lua_gen_cond_dep '
			dev-util/hermes[${LUA_SINGLE_USEDEP}]
		')
		app-shells/tcsh
	)
"

PATCHES=( "${FILESDIR}"/${PN}-8.4.19-no-libsandbox.patch )
LDIR="/etc/Lmod"
MDIR="${LDIR}/modules"
SDIR="${LDIR}/cacheDir"

pkg_setup() {
	lua-single_pkg_setup
	# get first two lua version numbers
	LUASHORT=`echo $(lua_get_version) | cut -d'.' -f 1-2`
}

src_prepare() {
	default
	rm -r pkgs/{luafilesystem,term} || die
	rm -r rt/{ck_mtree_syntax,colorize,end2end,help,ifur,settarg} || die
	hprefixify -w '/#\!\/bin\/tcsh/' rt/csh_swap/csh_swap.tdesc || die
	eautoreconf
}

src_configure() {
	local LMOD_SITENAME="${LMOD_SITENAME:-Liguros}"
	local LMOD_SYSHOST="${LMOD_SYSHOST:-Liguros}"

	local LUAC="${LUA%/*}/luac${LUA#*lua}"

	local myconf=(
		--with-tcl
		--with-fastTCLInterp
		--with-colorize
		--with-supportKsh
		--without-useBuiltinPkgs
		--with-siteControlPrefix
		--with-siteName="${LMOD_SITENAME}"
		--with-syshost="${LMOD_SYSHOST}"
		--with-lua_include="$(lua_get_include_dir)"
		--with-lua="${LUA}"
		--with-luac="${LUAC}"
		--with-module-root-path="${EPREFIX}/etc/modulefiles"
		--with-spiderCacheDir="${EPREFIX}/etc/lmod_cache/spider_cache"
		--with-updateSystemFn="${EPREFIX}/etc/lmod_cache/system.txt"
		--prefix="${EPREFIX}/usr/share/Lmod"
		--with-caseIndependentSorting
		--without-hiddenItalic
		--with-exportedModuleCmd
		--without-redirect
		--with-extendedDefault
		$(use_with cache cachedLoads)
		$(use_with duplicate-paths duplicatePaths)
		$(use_with auto-swap autoSwap)
	)
	econf "${myconf[@]}"
}

src_compile() {
	CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}" \
	default
}

src_test() {
	local -x PATH="${EPREFIX}/opt/hermes/bin:${PATH}"
	tm -vvv || die
	testcleanup || die
}

src_install() {
	dodir ${MDIR}
	dodir ${SDIR}
	doins -r "${FILESDIR}/etc"
	default
	# not a real man page
	rm -r "${ED}"/usr/share/Lmod/share/man || die
	keepdir /etc/modulefiles
	keepdir /etc/lmod_cache
}

pkg_postinst() {
	elog "You should add the following to your ~/.bashrc to use Lmod:"
	elog "export MODULEPATH=/usr/lmod/lmod/modulefiles:${MDIR}"
	elog "[ -f /usr/lmod/lmod/init/bash ] && \\ "
	elog "    source /usr/lmod/lmod/init/bash"
	elog ""
	elog "You may also want to run the following commands to update the system cache dir:"
	elog "export MODULEPATH=/usr/lmod/lmod/modulefiles:${MDIR}"
	elog "update_lmod_system_cache_files -d ${SDIR} -t ${MDIR}/system.txt $MODULEPATH"
}
