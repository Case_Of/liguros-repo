# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

CMAKE_IN_SOURCE_BUILD=1
inherit cmake

DESCRIPTION="Synchronous multi-master replication engine that provides the wsrep API"
HOMEPAGE="https://galeracluster.com"
SRC_URI="https://gitlab.com/liguros/distfiles/-/raw/main/${P}.tar.gz"
LICENSE="GPL-2 BSD"

SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ia64 ~ppc ~ppc64 ~x86"
IUSE="cpu_flags_x86_sse4_2 garbd libressl test"

# Tests are currently broken, see
#   - https://github.com/codership/galera/issues/595
#   - https://github.com/codership/galera/issues/596
RESTRICT="test"

COMMON_DEPEND="
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:= )
	>=dev-libs/boost-1.41:0=
"

DEPEND="
	${COMMON_DEPEND}
	dev-libs/check
	>=dev-cpp/asio-1.10.1[ssl(+)]
"

#Run time only
RDEPEND="${COMMON_DEPEND}"

S="${WORKDIR}/${PN}-4-${PV}"

src_prepare() {
	default
	eapply "${FILESDIR}"/${PN}-26.4.8-libressl.patch
	eapply "${FILESDIR}"/${PN}-26.4.6-strip-extra-cflags.patch
	eapply "${FILESDIR}"/${PN}-26.4.8-respect-toolchain.patch

	# Remove bundled dev-cpp/asio
	rm -r "${S}/asio" || die "Failed to remove bundled asio"

	cmake_src_prepare
}

src_configure() {
	tc-export AR CC CXX OBJDUMP

	local mycmakeargs=(
		-DHAVE_SYSTEM_ASIO_HPP=1
	)
	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_install() {
	dodoc scripts/packages/README scripts/packages/README-MySQL
	if use garbd ; then
		dobin garb/garbd
		newconfd "${FILESDIR}/garb.cnf" garbd
		newinitd "${FILESDIR}/garb.init" garbd
		doman man/garbd.8
	fi
	exeinto /usr/$(get_libdir)/"${PN}"
	doexe libgalera_smm.so
}
