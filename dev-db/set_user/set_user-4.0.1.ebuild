# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
POSTGRES_COMPAT=( 13 14 15 16 )

inherit postgres

MY_PV=$(ver_rs 1- _)

DESCRIPTION="PostgreSQL extension allowing privilege escalation"
HOMEPAGE="https://github.com/pgaudit/set_user"
SRC_URI="https://github.com/pgaudit/set_user/archive/refs/tags/REL${MY_PV}.tar.gz"

LICENSE="PostgreSQL"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	dev-db/postgresql
"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md RELEASENOTES.md )

S="${WORKDIR}/${PN}-REL${MY_PV}"

src_compile() {
	emake USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}

src_install() {
	emake DESTDIR="${D}" install USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}
