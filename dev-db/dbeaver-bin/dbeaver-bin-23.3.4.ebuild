# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
inherit desktop wrapper xdg-utils

DESCRIPTION="Free universal database tool (community edition)."
HOMEPAGE="https://dbeaver.io/"
MY_PN="${PN%-bin*}-ce"
SRC_URI="
	amd64? ( https://dbeaver.io/files/${PV}/${MY_PN}-${PV}-linux.gtk.x86_64-nojdk.tar.gz -> ${P}-amd64.tar.gz )
	arm64? ( https://dbeaver.io/files/${PV}/${MY_PN}-${PV}-linux.gtk.aarch64-nojdk.tar.gz  -> ${P}-arm64.tar.gz )
"

LICENSE="Apache-2.0 EPL-1.0 BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64"

RDEPEND="virtual/jre:17"
DEPEND="${RDEPEND}"

MY_PN="${PN%-bin*}"
S="${WORKDIR}/${MY_PN}"

src_prepare() {
	default
}

src_install() {
	doicon -s 128 "${MY_PN}.png"
	newicon icon.xpm "${MY_PN}.xpm"
	einstalldocs
	rm "${MY_PN}.png" icon.xpm readme.txt
	insinto "/opt/${MY_PN}"
	doins -r *
	fperms 0755 "/opt/${MY_PN}/${MY_PN}"
	make_wrapper "${MY_PN}" "/opt/${MY_PN}/${MY_PN}" "/opt/${MY_PN}"
	sed -e "s:^exec /opt/${MY_PN}/${MY_PN}:exec /opt/${MY_PN}/${MY_PN} -vm ${EPREFIX}/opt/openjdk-bin-17/bin:" \
		-i "${D}/usr/bin/${MY_PN}"
}

pkg_postinst() {
	# Update mimedb for the new .desktop file
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
