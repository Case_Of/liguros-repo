# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
POSTGRES_COMPAT=( 13 14 15 16 )

MY_PV=${PV/_/-}

DESCRIPTION="PostgreSQL Database Modeler"
HOMEPAGE="https://pgmodeler.io"
SRC_URI="https://github.com/pgmodeler/pgmodeler/archive/v${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="${POSTGRES_DEP}
	dev-libs/icu:=
	dev-libs/libxml2:=
	dev-qt/qtbase:6
	dev-qt/qtsvg:6
	dev-qt/qt5compat:6
	dev-libs/libxml2
	dev-db/postgresql
"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md RELEASENOTES.md )

S="${WORKDIR}/${PN}-${MY_PV}"

src_configure() {
	# Running qmake6 directly until eqmake6 is available
	qmake6 \
	PREFIX="${EPREFIX}/usr" \
	PLUGINSDIR="${EPREFIX}/usr/$(get_libdir)/${PN}/plugins" \
	PRIVATEBINDIR="${EPREFIX}/usr/$(get_libdir)/${PN}/bin" \
	PRIVATELIBDIR="${EPREFIX}/usr/$(get_libdir)/${PN}" \
	NO_UPDATE_CHECK=1 \
	-r ${PN}.pro
}

src_install() {
	emake INSTALL_ROOT="${D}" install

	rm "${D}"/usr/share/${PN}/{CHANGELOG.md,LICENSE,README.md,RELEASENOTES.md}

	einstalldocs

	doicon assets/conf/${PN}_logo.png
	make_desktop_entry ${PN} ${PN} ${PN}_logo Development
}
