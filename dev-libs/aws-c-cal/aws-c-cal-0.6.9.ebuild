# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="Crypto Abstraction Layer: Cross-Platform C99 wrapper for cryptography primitives"
HOMEPAGE="https://github.com/awslabs/aws-c-cal"
SRC_URI="https://github.com/awslabs/aws-c-cal/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="libressl ssl static-libs test"

RESTRICT="!test? ( test )"

DEPEND="
	>=dev-libs/aws-c-common-0.4.62:=
	!libressl? ( >=dev-libs/openssl-1.1.1:=[static-libs=] )
	libressl? ( dev-libs/libressl )
"

PATCHES=(
	"${FILESDIR}"/${PN}-0.5.12-cmake-prefix.patch
	"${FILESDIR}"/${PN}-0.5.12-add_libz_for_static.patch
)

src_prepare() {
	if use libressl; then
		eapply ${FILESDIR}/source_unix_openssl_rsa_c.patch
	fi

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBYO_CRYPTO=$(usex !ssl)
		-DUSE_OPENSSL=$(usex ssl)
		-DBUILD_SHARED_LIBS=$(usex !static-libs)
		-DBUILD_TESTING=$(usex test)
	)
	cmake_src_configure
}
