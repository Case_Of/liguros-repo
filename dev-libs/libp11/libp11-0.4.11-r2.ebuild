# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Abstraction layer to simplify PKCS#11 API"
HOMEPAGE="https://github.com/opensc/libp11/wiki"
SRC_URI="https://github.com/OpenSC/${PN}/releases/download/${P}/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sparc ~x86"
IUSE="libressl bindist doc static-libs"

RDEPEND="
	!libressl? ( dev-libs/openssl:0=[bindist=] )
	libressl? ( >=dev-libs/libressl-3.5.0:0= )"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? ( app-text/doxygen )"

PATCHES="
	${FILESDIR}/p11_cert.patch
	${FILESDIR}/libp11_int.patch
	${FILESDIR}/p11_key.patch
	${FILESDIR}/p11_ec.patch
	${FILESDIR}/p11_rsa.patch
	${FILESDIR}/p11_pkey.patch
	${FILESDIR}/auth.patch
	${FILESDIR}/decrypt.patch
"

src_configure() {
	econf \
		--enable-shared \
		$(use_enable static-libs static) \
		$(use_enable doc api-doc)
}

src_install() {
	default
	find "${D}" -name '*.la' -delete || die
}
