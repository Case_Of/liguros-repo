# Copyright 2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake elisp-common

DESCRIPTION="Google's Protocol Buffers - Extensible mechanism for serializing structured data"
HOMEPAGE="https://developers.google.com/protocol-buffers/"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~loong ~mips ppc ppc64 ~riscv ~s390 sparc x86 ~amd64-linux ~x86-linux ~x64-macos"
GOOGLETEST_COMMIT="4c9a3bb62bf3ba1f1010bf96f9c8ed767b363774"
SRC_URI="
	https://github.com/protocolbuffers/protobuf/archive/refs/tags/v${PV}.tar.gz
	https://github.com/google/googletest/archive/${GOOGLETEST_COMMIT}.tar.gz -> googletest-${GOOGLETEST_COMMIT}.tar.gz
"

LICENSE="BSD"
SLOT="0/23"
IUSE="emacs examples test zlib"
RESTRICT="!test? ( test )"

BDEPEND="
	=dev-cpp/abseil-cpp-20230125.3-r1
"
DEPEND="
	emacs? ( app-editors/emacs:* )
	test? ( >=dev-cpp/gtest-1.9 )
	zlib? ( sys-libs/zlib )
"
RDEPEND="
	${DEPEND}
	zlib? ( sys-libs/zlib )
"

DOCS=(CONTRIBUTORS.txt README.md)

src_prepare() {
	cp -r ${WORKDIR}/googletest-${GOOGLETEST_COMMIT}/* ${S}/third_party/googletest
	rm -rf ${WORKDIR}/googletest-${GOOGLETEST_COMMIT}
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-Dprotobuf_ABSL_PROVIDER=package
		-Dprotobuf_BUILD_TESTS=OFF
	)

	cmake_src_configure
}

src_install() {
	insinto /usr/share/vim/vimfiles/syntax
	doins editors/proto.vim
	insinto /usr/share/vim/vimfiles/ftdetect
	doins "${FILESDIR}/proto.vim"

	if use emacs; then
		elisp-install ${PN} editors/protobuf-mode.el*
		elisp-site-file-install "${FILESDIR}/70${PN}-gentoo.el"
	fi

	if use examples; then
		DOCS+=(examples)
		docompress -x /usr/share/doc/${PF}/examples
	fi

	cmake_src_install
}

pkg_postinst() {
	use emacs && elisp-site-regen
}

pkg_postrm() {
	use emacs && elisp-site-regen
}
