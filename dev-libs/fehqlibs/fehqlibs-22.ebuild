# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="State-of-the-art C routines provided as library for Internet services"
HOMEPAGE="https://www.fehcom.de/ipnet/qlibs.html"
SRC_URI="http://www.fehcom.de/ipnet/fehQlibs/fehQlibs-${PV}.tgz"
LICENSE=""

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86 ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~sparc-solaris ~x64-solaris ~x86-solaris"
IUSE=""

DEPEND=""
S="${WORKDIR}/fehQlibs-${PV}"

PATCHES="${FILESDIR}/Makefile.patch"
DOCS="LICENSE README.md"

src_configure() {
	cd ${S}
	./configure
}

src_compile() {
	cd ${S}
	make libs
}

src_install() {
	cd ${S}
	dolib.a dnsresolv.a libdnsresolv.a
	dolib.a qlibs.a libqlibs.a
	mv include fehqlibs
	doheader -r fehqlibs
	default
}
