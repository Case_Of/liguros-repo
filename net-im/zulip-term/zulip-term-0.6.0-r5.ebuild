# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="The official terminal client for Zulip"
HOMEPAGE="https://github.com/zulip/zulip-terminal"
GIT_COMMIT="a03ae750d091c35e46929804d6c767ee7bdec7e7"
SRC_URI="https://github.com/zulip/zulip-terminal/archive/${GIT_COMMIT}.tar.gz -> ${PN}-${GIT_COMMIT}.tar.gz"
S=${WORKDIR}/zulip-terminal-${GIT_COMMIT}
LICENSE="Apache 2.0"
SLOT="0"
KEYWORDS="*"
IUSE="python"
RESTRICT="test"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

DEPEND="
	${PYTHON_DEPS}
	>=dev-python/urwid-2.1.2
	>=dev-python/urwid_readline-0.12
	>=dev-python/tzlocal-2.1
	dev-python/python-dateutil
	dev-python/typing-extensions
	dev-python/beautifulsoup4
	dev-python/lxml
	>=dev-python/pyperclip-1.8.1
	>=dev-python/zulip-api-0.7.0
	dev-python/matrix-client
"
RDEPEND="${DEPEND}"

src_prepare() {
	# create missing init file of zulipterminal.themes module
	touch ${S}/zulipterminal/themes/__init__.py
	distutils-r1_src_prepare
}

python_install_all() {
	distutils-r1_python_install_all
	python_optimize
}
