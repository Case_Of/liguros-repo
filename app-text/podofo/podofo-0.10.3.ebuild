# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit cmake flag-o-matic lua-single toolchain-funcs

DESCRIPTION="PoDoFo is a C++ library to work with the PDF file format"
HOMEPAGE="https://sourceforge.net/projects/podofo/"

RESOURCES_COMMIT="1afc745452245cc75640682d1ec36a4a383756cd"
SRC_URI="
	https://github.com/podofo/podofo/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/podofo/podofo-resources/archive/${RESOURCES_COMMIT}.tar.gz
"

LICENSE="GPL-2 LGPL-2.1"
SLOT="0/${PV%_*}"
KEYWORDS="amd64 ~arm ~arm64 ~hppa ppc ppc64 ~sparc x86"
IUSE="+boost idn libressl debug test +tools"
RESTRICT="!test? ( test )"
REQUIRED_USE="${LUA_REQUIRED_USE}
	test? ( tools )"

RDEPEND="${LUA_DEPS}
	idn? ( net-dns/libidn:= )
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
	media-libs/fontconfig:=
	media-libs/freetype:2=
	virtual/jpeg:0=
	media-libs/libpng:0=
	media-libs/tiff:0=
	sys-libs/zlib:="
DEPEND="${RDEPEND}
	virtual/pkgconfig
	boost? ( dev-libs/boost )
	test? ( dev-util/cppunit )"

DOCS="AUTHORS.md CHANGELOG.md TODO.md"

src_prepare() {
	cp -fr ${WORKDIR}/podofo-resources-${RESOURCES_COMMIT}/* ${S}/extern/resources
	rm -rf ${WORKDIR}/podofo-resources-${RESOURCES_COMMIT}
	cmake_src_prepare

	if use libressl; then
		sed -e 's:^#ifdef PODOFO_HAVE_OPENSSL_1_1$:#ifndef PODOFO_HAVE_OPENSSL_1_1:' \
			 -i tools/podofosign/podofosign.cpp || die #663602
	fi
}

src_configure() {
	# Bug #381359: undefined reference to `PoDoFo::PdfVariant::DelayedLoadImpl()'
	filter-flags -fvisibility-inlines-hidden

	mycmakeargs+=(
		"-DPODOFO_BUILD_STATIC=0"
		"-DPODOFO_HAVE_JPEG_LIB=1"
		"-DPODOFO_HAVE_PNG_LIB=1"
		"-DPODOFO_HAVE_TIFF_LIB=1"
		"-DWANT_FONTCONFIG=1"
		"-DUSE_STLPORT=0"
		-DLUA_VERSION="$(lua_get_version)"
		-DWANT_BOOST=$(usex boost ON OFF)
		-DHAVE_LIBIDN=$(usex idn ON OFF)
		-DPODOFO_HAVE_CPPUNIT=$(usex test ON OFF)
		-DPODOFO_BUILD_LIB_ONLY=$(usex tools OFF ON)
		)

	cmake_src_configure
	mkdir -p "${S}/test/TokenizerTest/objects" || die
}

src_test() {
	cd "${BUILD_DIR}"/test/unit || die
	./podofo-test --selftest || die "self test failed"
}
