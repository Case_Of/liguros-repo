# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit xdg-utils

DESCRIPTION="BitTorrent client in C++ and Qt"
HOMEPAGE="https://www.qbittorrent.org https://github.com/qbittorrent"

SRC_URI="https://github.com/qbittorrent/qBittorrent/archive/refs/tags/release-${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 ~arm ~ppc64 x86"
S="${WORKDIR}/qBittorrent-release-${PV}"
LICENSE="GPL-2"
SLOT="0"
IUSE="+dbus debug geoip +gui webui"
REQUIRED_USE="dbus? ( gui )"

RDEPEND="
	>=dev-libs/boost-1.62.0-r1:=
	dev-qt/qtcore:5
	dev-qt/qtnetwork:5[ssl]
	dev-qt/qtxml:5
	dev-qt/qtsql:5
	>=net-libs/libtorrent-rasterbar-1.2.12
	sys-libs/zlib
	dbus? ( dev-qt/qtdbus:5 )
	gui? (
		geoip? ( dev-libs/geoip )
		dev-qt/qtgui:5
		dev-qt/qtsvg:5
		dev-qt/qtwidgets:5
	)"

DEPEND="${RDEPEND}
	dev-qt/linguist-tools:5"

BDEPEND="virtual/pkgconfig"

DOCS=( AUTHORS Changelog CONTRIBUTING.md README.md )

src_configure() {
	econf \
	$(use_enable dbus qt-dbus) \
	$(use_enable debug) \
	$(use_enable webui) \
	$(use_enable gui gui)
}

src_install() {
	emake STRIP="/bin/false" INSTALL_ROOT="${D}" install
	einstalldocs
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}
