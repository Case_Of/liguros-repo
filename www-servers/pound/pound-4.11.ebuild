# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="A http/https reverse-proxy and load-balancer"
HOMEPAGE="https://github.com/graygnuorg/pound"
SRC_URI="https://github.com/graygnuorg/pound/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
IUSE="libressl"

LICENSE="BSD GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-libs/libpcre:=
	dev-libs/libyaml:=
	dev-libs/nanomsg:=
	!libressl? ( dev-libs/openssl:0 )
	libressl? ( dev-libs/libressl )
	<net-libs/mbedtls-3.0.0
"

RDEPEND="${DEPEND}"

DOCS=( README.md )
#PATCHES=( ${FILESDIR}/src_svc_c.patch )

src_prepare() {
	cd ${S}
	eapply -p1 ${FILESDIR}/limits.patch
	eapply -p0 ${FILESDIR}/src_ht_h.patch
	eapply -p0 ${FILESDIR}/src_pound_c.patch
	./bootstrap
	default
}

src_configure() {
	default
}

src_compile() {
	default
}

src_install() {
	default
#	dosbin "${BUILD_DIR}"/pound
#	doman "${S}"/man/pound.8
#	einstalldocs
#
#	dodir /etc/init.d
#	newinitd "${FILESDIR}"/pound.init-1.9 pound
#
#	insinto /etc
#	newins "${FILESDIR}"/pound-2.2.cfg pound.cfg
}
