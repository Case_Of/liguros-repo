# Copyright 2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Small tool to create CRATES entries for gentoo ebuilds from a Cargo.lock file."
HOMEPAGE="https://gitlab.com/farout/cargolock2crates"
SRC_URI="${HOMEPAGE}/-/archive/${PV}/${P}.tar.gz"

inherit golang-base

LICENSE="MIT"
SLOT="0"
KEYWORDS="*"

BDEPEND="dev-lang/go"

DOCS=( README.md )

program_make() {
	local my_tags=(
		prod
	)
	local my_makeopt=(
		TAGS="${my_tags[@]}"
		LDFLAGS="-extldflags \"${LDFLAGS}\""
	)
	emake "${my_makeopt[@]}" "$@"
}

src_compile() {
	program_make build
}

src_install() {
	# Install binary
	dobin ${PN}

	# Install docs
	einstalldocs
}
