# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="Errcheck checks that you checked errors"
SRC_URI="
	https://github.com/kisielk/errcheck/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"
HOMEPAGE="https://github.com/kisielk/errcheck"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 arm x86"

DEPEND="!dev-go/${PN}"

src_compile() {
	cd ${S}
	mkdir bin
	go build -o bin/${PN} main.go
}

src_install() {
	cd ${S}
	# Install binary
	dobin bin/${PN}

	# Install docs
	einstalldocs
}
