# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit bash-completion-r1 meson toolchain-funcs

# From libr/asm/arch/arm/v35arm64/Makefile
VECTOR35_ARCH_ARM64_COMMIT="c9e7242972837ac11fc94db05fabcb801a8269c9"
VECTOR35_ARCH_ARMV7_COMMIT="f270a6cc99644cb8e76055b6fa632b25abd26024"
BINS_COMMIT="1f0ea409f2af83fc4d0b685855de9d3b0f9eee41"

DESCRIPTION="unix-like reverse engineering framework and commandline tools"
HOMEPAGE="http://www.radare.org"
SRC_URI="
	https://github.com/radareorg/radare2/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	test? ( https://github.com/radareorg/radare2-testbins/archive/${BINS_COMMIT}.tar.gz -> radare2-testbins-${BINS_COMMIT}.tar.gz )
	https://github.com/radareorg/vector35-arch-arm64/archive/${VECTOR35_ARCH_ARM64_COMMIT}.tar.gz -> vector35-arch-arm64-${VECTOR35_ARCH_ARM64_COMMIT}.tar.gz
	https://github.com/radareorg/vector35-arch-armv7/archive/${VECTOR35_ARCH_ARMV7_COMMIT}.tar.gz -> vector35-arch-armv7-${VECTOR35_ARCH_ARMV7_COMMIT}.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="libressl ssl test"

RDEPEND="
	>=dev-libs/capstone-5.0_rc4:=
	dev-libs/libzip:=
	dev-libs/xxhash
	sys-apps/file
	sys-libs/zlib
	>=dev-libs/libuv-1.0.0
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)
"
DEPEND="
	${RDEPEND}
	dev-util/gperf
"
BDEPEND="virtual/pkgconfig"

PATCHES=( ${FILESDIR}/capstone_meson_build.patch )

src_prepare() {
	mv "${WORKDIR}/vector35-arch-arm64-${VECTOR35_ARCH_ARM64_COMMIT}" "${S}/libr/arch/p/arm/v35/arch-arm64" || die
	mv "${WORKDIR}/vector35-arch-armv7-${VECTOR35_ARCH_ARMV7_COMMIT}" "${S}/libr/arch/p/arm/v35/arch-armv7" || die

	if use test; then
		cp -r "${WORKDIR}/radare2-testbins-${BINS_COMMIT}" "${S}/test/bins" || die
		cp -r "${WORKDIR}/radare2-testbins-${BINS_COMMIT}" "${S}" || die
	fi

	# Fix hardcoded docdir for fortunes
	sed -i -e "/^#define R2_FORTUNES/s/radare2/$PF/" \
		libr/include/r_userconf.h.acr || die
	default
}

src_configure() {
	tc-export CC AR LD OBJCOPY RANLIB
	export HOST_CC=${CC}

	local mesonargs=(
		-Duse_sys_capstone=true
		-Duse_sys_magic=true
		-Duse_sys_xxhash=true
		-Duse_sys_zlib=true
		$(meson_use ssl)
	)
	meson_src_configure
}

src_test() {
	emake -C test -k unit_tests || die
}

src_install() {
	meson_src_install
}
