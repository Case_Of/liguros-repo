# Copyright 2020-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit go-module

DESCRIPTION="Inspects golang source code for security problems"
HOMEPAGE="https://securego.io/"
SRC_URI="
	https://github.com/securego/gosec/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 arm x86"

pkg_setup() {
	GOLANG_PKG_LDFLAGS+=" -X main.BuildDate=$( date +%F )"
}

src_compile() {
	default
	make build-linux
}

src_install() {
	dobin gosec
	default
}
