# Copyright 2020-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

#EGO_PN="github.com/GoogleContainerTools/${PN}/..."
SRC_URI="
	https://github.com/GoogleContainerTools/container-diff/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-deps.tar.xz
"

inherit go-module

DESCRIPTION="tool for analyzing and comparing container images"
HOMEPAGE="https://github.com/GoogleContainerTools/container-diff"

#EGIT_REPO_URI="https://github.com/GoogleContainerTools/container-diff.git"
#EGIT_COMMIT="v${PV}"
#EGIT_CHECKOUT_DIR="${S}"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm"
IUSE=""

DEPEND=""

src_compile() {
	cd ${S}
	mkdir bin
	go build -v -o bin/${PN} main.go
}

src_install() {
	cd ${S}
	dobin bin/${PN}
}
