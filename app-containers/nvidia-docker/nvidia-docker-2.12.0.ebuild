# Copyright 2020-2023 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="NVIDIA Docker"
HOMEPAGE="https://github.com/NVIDIA/nvidia-docker"
SRC_URI="https://github.com/NVIDIA/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND=""

RDEPEND="${DEPEND}
	app-containers/docker
	>=app-containers/nvidia-container-toolkit-1.7.0
	x11-drivers/nvidia-drivers"

src_compile() {
	:
}

src_install() {
	dobin ${PN}
	dodir /etc/docker
	insinto /etc/docker
	doins daemon.json
}
