# Copyright 2021-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PHP_EXT_NAME="swoole"
PHP_EXT_INI="yes"
PHP_EXT_ZENDEXT="no"
PHP_EXT_S="${WORKDIR}/${PN}-src-${PV}"
PHP_EXT_SAPIS="cli"
PHP_EXT_NEEDED_USE="cli,sockets?"
USE_PHP="php8-0 php8-1 php8-2 php8-3"

inherit php-ext-source-r3

HOMEPAGE="https://www.swoole.co.uk"
SRC_URI="https://github.com/swoole/swoole-src/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

DOCS=( README.md )

DESCRIPTION="Event-driven asynchronous & concurrent & coroutine networking engine"
LICENSE="Apache-2.0"
SLOT="0"

DEPEND="
	app-arch/brotli:0=
	dev-libs/libpcre
	sys-libs/zlib:0=
	ssl? (
		!libressl? ( dev-libs/openssl:0= )
		libressl? ( dev-libs/libressl:0= )
	)
	mysql? (
		php_targets_php8-0? ( dev-lang/php:8.0[mysql,mysqli(+)] )
		php_targets_php8-1? ( dev-lang/php:8.1[mysql,mysqli(+)] )
		php_targets_php8-2? ( dev-lang/php:8.2[mysql,mysqli(+)] )
		php_targets_php8-3? ( dev-lang/php:8.3[mysql,mysqli(+)] )
	)
"

RDEPEND="${DEPEND}"
S="${WORKDIR}/${PN}-src-${PV}"
IUSE="debug http2 libressl mysql sockets ssl"

src_prepare() {
	php-ext-source-r3_src_prepare
}

src_configure() {
	local PHP_EXT_ECONF_ARGS=(
		--enable-swoole
		$(use_enable debug)
		$(use_enable http2)
		$(use_enable mysql mysqlnd)
		$(use_enable ssl openssl)
		$(use_with ssl openssl-dir "${EROOT}/usr")
		$(use_enable sockets)
	)
	php-ext-source-r3_src_configure
}

src_install() {
	php-ext-source-r3_src_compile
}

src_install() {
	php-ext-source-r3_src_install
}

src_test() {
	php-ext-source-r3_src_test
}
