# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

WX_GTK_VER="3.0-gtk3"
LUA_COMPAT=( lua5-{1,3,4} luajit )

inherit wxwidgets xdg lua-single

DESCRIPTION="Reverse Engineers' Hex Editor"
HOMEPAGE="https://github.com/solemnwarning/rehex"
SRC_URI="https://github.com/solemnwarning/rehex/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"

RESTRICT="mirror"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc"

RDEPEND="
	${LUA_DEPS}
	dev-libs/capstone
	dev-libs/jansson
	x11-libs/wxGTK:${WX_GTK_VER}[X]
"
BDEPEND="
	virtual/pkgconfig
	dev-lua/busted
	app-arch/zip
	doc? ( dev-perl/Template-Toolkit )
"
DEPEND="
	${RDEPEND}
	${BDEPEND}
"

src_configure() {
	export LUA_PKG=${ELUA}
	if use !doc ; then
		export BUILD_HELP=0
	fi
	setup-wxwidgets
}

src_install() {
	emake LUA_PKG=${ELUA} prefix="${D}"/usr install
}
