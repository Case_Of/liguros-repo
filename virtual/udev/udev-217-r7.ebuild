# Copyright 2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Virtual to select between different udev daemon providers"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~loong ~m68k ~mips ppc ppc64 ~riscv ~s390 sparc x86"
IUSE="systemd sticky-tags eudev"
REQUIRED_USE="
	?? ( eudev systemd )
	eudev? ( !sticky-tags )
"

RDEPEND="
	systemd? ( >=sys-apps/systemd-217 )
	!systemd? (
		|| (
			eudev? ( >=sys-fs/eudev-2.1.1 )
			!eudev? ( sys-apps/systemd-utils[udev] )
		)
	)
"
