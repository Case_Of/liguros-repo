# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Text input widget for urwid that supports readline shortcuts."
HOMEPAGE="https://pypi.org/project/urwid-readline/ https://github.com/rr-/urwid_readline"
SRC_URI="https://github.com/rr-/urwid_readline/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

src_prepare() {
	distutils-r1_src_prepare
}

python_install_all() {
	distutils-r1_python_install_all
	python_optimize
}
