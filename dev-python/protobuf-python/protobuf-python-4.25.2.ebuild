# Copyright 2023-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1 flag-o-matic

PARENT_PN="${PN/-python/}"
PARENT_PV="$(ver_cut 2-)"
PARENT_P="${PARENT_PN}-${PARENT_PV}"

SRC_URI="
	https://github.com/protocolbuffers/protobuf/archive/v${PARENT_PV}.tar.gz -> ${PARENT_P}.tar.gz
"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~loong ~mips ~ppc64 ~riscv ~x86 ~amd64-linux ~x86-linux ~x64-macos"

DESCRIPTION="Google's Protocol Buffers - Python bindings"
HOMEPAGE="
	https://developers.google.com/protocol-buffers/
	https://pypi.org/project/protobuf/
"

LICENSE="BSD"
SLOT="0/25.2.0"

S="${WORKDIR}/${PARENT_P}/python"

BDEPEND="
"
DEPEND="
	${PYTHON_DEPS}
"
RDEPEND="
	${BDEPEND}
	dev-libs/protobuf:${SLOT}
"

distutils_enable_tests setup.py

python_prepare_all() {
	eapply_user

	distutils-r1_python_prepare_all
}

src_configure() {
	append-cxxflags -std=c++17
	DISTUTILS_ARGS=( --cpp_implementation )
}

python_compile() {
	distutils-r1_python_compile
	find "${BUILD_DIR}/install" -name "*.pth" -type f -delete || die
}
