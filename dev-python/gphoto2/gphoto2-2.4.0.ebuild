# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
DISTUTILS_USE_SETUPTOOLS=no

inherit distutils-r1

DESCRIPTION="python-gphoto2 is a comprehensive Python binding to libgphoto2"
HOMEPAGE="https://pypi.org/project/gphoto2 https://github.com/jim-easterbrook/python-gphoto2"
SRC_URI="https://github.com/jim-easterbrook/python-gphoto2/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	media-libs/libgphoto2
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/python-${P}
