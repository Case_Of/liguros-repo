# Copyright 2020-2023 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Generate distribution packages from PyPI"
HOMEPAGE="https://github.com/openSUSE/py2pack https://pypi.org/project/py2pack/"
SRC_URI="https://files.pythonhosted.org/packages/f2/3a/aefdc848191ca962bf8f794b7ed2f0cc783580dcae4fad84334617014f85/py2pack-0.9.0.tar.gz"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND=""

DEPEND="${RDEPEND}
	dev-python/jinja
	dev-python/six
	dev-python/metaextract
	dev-python/pbr
	dev-python/setuptools[${PYTHON_USEDEP}]
	"
