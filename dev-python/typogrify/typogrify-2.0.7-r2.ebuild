# Copyright 2020-2023 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Filters to enhance web typography, often used with Jinja or Django"
SRC_URI="https://files.pythonhosted.org/packages/8a/bf/64959d6187d42472acb846bcf462347c9124952c05bd57e5769d5f28f9a6/typogrify-2.0.7.tar.gz"
HOMEPAGE="https://github.com/mintchaos/typogrify https://pypi.python.org/pypi/typogrify/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="*"

DEPEND=${PYTHON_DEPS}
RDEPEND="${PYTHON_DEPS}
	dev-python/smartypants"

RESTRICT="mirror"

DOCS="README.rst"
