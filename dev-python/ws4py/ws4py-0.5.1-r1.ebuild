# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
PYTHON_REQ_USE="threads(+)?"

inherit distutils-r1

DESCRIPTION="WebSocket client and server library for Python 2 and 3 as well as PyPy"
HOMEPAGE="https://github.com/Lawouach/WebSocket-for-Python"
MY_PN="WebSocket-for-Python"
SRC_URI="https://github.com/Lawouach/${MY_PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 arm x86"
LICENSE="BSD"
SLOT="0"
IUSE="+client +server test +threads"
RESTRICT="!test? ( test )"

RDEPEND="
	>=dev-python/greenlet-0.4.1[${PYTHON_USEDEP}]
	>=dev-python/cython-0.19.1[${PYTHON_USEDEP}]
	client? ( >=dev-python/tornado-3.1[${PYTHON_USEDEP}] )
	server? ( >=dev-python/cherrypy-3.2.4[${PYTHON_USEDEP}] )
"
DEPEND="
	test? (
		>=dev-python/cherrypy-3.2.4[${PYTHON_USEDEP}]
		>=dev-python/mock-1.0.1[${PYTHON_USEDEP}]
	)
"

S="${WORKDIR}/${MY_PN}-${PV}"

PATCHES=(
	"${FILESDIR}"/${PN}-0.5.1-python3.7+-compatibility.patch
)

python_test() {
	"${EPYTHON}" -m unittest discover || die "Tests failed under ${EPYTHON}"
}

python_install() {
	distutils-r1_python_install
	use client || rm -rf "${D}$(python_get_sitedir)"/ws4py/client
	use server || rm -rf "${D}$(python_get_sitedir)"/ws4py/server
}
