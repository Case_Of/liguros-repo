# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8,9,10,11,12} pypy3 )

inherit distutils-r1 pypi

DESCRIPTION="A WSGI middleware that secures routes using HTTP Digest Authentication"
HOMEPAGE="
	https://github.com/jonashaag/httpauth/
	https://pypi.org/project/httpauth/
"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

PATCHES=(
	"${FILESDIR}/${P}-remove-nose-dependency.patch"
)

distutils_enable_tests pytest

python_test() {
	epytest tests.py
}
