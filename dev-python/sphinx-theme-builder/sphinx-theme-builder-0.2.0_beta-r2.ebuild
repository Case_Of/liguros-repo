# Copyright 2022-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
PYTHON_REQ_USE="ncurses"
RESTRICT="test"
MY_PV=${PV/_beta/b2}

DISTUTILS_USE_PEP517=flit
inherit distutils-r1

DESCRIPTION="A tool for authoring Sphinx themes with a simple (opinionated) workflow"
HOMEPAGE="https://pypi.org/project/sphinx-theme-builder/ https://github.com/pradyunsg/sphinx-theme-builder"
SRC_URI="https://github.com/pradyunsg/sphinx-theme-builder/archive/refs/tags/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

DEPEND="
	dev-python/pyproject-metadata
"

S=${WORKDIR}/${PN}-${MY_PV}

distutils_enable_sphinx docs
distutils_enable_tests setup.py

