# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Read and write Ruby-marshalled data"
HOMEPAGE="https://github.com/d9pouces/RubyMarshal"
SRC_URI="https://github.com/d9pouces/RubyMarshal/archive/${PV}.tar.gz"

SLOT="0"
LICENSE="WTFPL"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux ~amd64-fbsd"

#DEPEND="dev-libs/libversion"

S="${WORKDIR}/RubyMarshal-${PV}"
