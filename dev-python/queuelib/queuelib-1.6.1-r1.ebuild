# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v3.0
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Collection of persistent (disk-based) queues"
HOMEPAGE="https://github.com/scrapy/queuelib"
SRC_URI="https://files.pythonhosted.org/packages/28/c3/c4371429aac06c9d6e1eae84a711b0ba61219b2ca7722db075c8abe9e088/queuelib-1.6.1.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND=""
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]"
