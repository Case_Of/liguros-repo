# Copyright 2020-2022 by Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit python-r1
DESCRIPTION="Utilities for using XStatic in Tornado applications"
HOMEPAGE="https://github.com/takluyver/tornado_xstatic"
SRC_URI="https://github.com/takluyver/tornado_xstatic/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"
S=${WORKDIR}/tornado_xstatic-${PV}

RDEPEND="dev-python/tornado[${PYTHON_USEDEP}] ${PYTHON_DEPS}"
DEPEND=""

src_install() {
	# no setup.py. We will create a hack, so that's packages using setup.py can find tornado-xstatic in their reqs.
	touch "${T}/tornado_xstatic-${PV}.egg-info"
	python_foreach_impl python_domodule tornado_xstatic.py "${T}/tornado_xstatic-${PV}.egg-info"
}
