# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
DISTUTILS_USE_SETUPTOOLS=bdepend

inherit distutils-r1

distutils_enable_tests unittest

DESCRIPTION="Python bindings for libversion"
HOMEPAGE="https://github.com/repology/py-libversion https://pypi.org/project/libversion/"
SRC_URI="https://github.com/repology/py-${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/py-${P}"
SLOT="0"
LICENSE="MIT"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux ~amd64-fbsd"

DEPEND="
	>=dev-libs/libversion-2.7.0
"
RDEPEND="
	${DEPEND}
"
BDEPEND="
	virtual/pkgconfig
"

python_compile(){
	PKG_CONFIG_PATH="${EPREFIX}/usr/lib/pkgconfig" distutils-r1_python_compile
}

python_install(){
	PKG_CONFIG_PATH="${EPREFIX}/usr/lib/pkgconfig" distutils-r1_python_install
}
