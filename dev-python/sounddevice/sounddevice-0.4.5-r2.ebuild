# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{8,9,10,11,12} )
inherit distutils-r1

DESCRIPTION="Python bindings for PortAudio"
HOMEPAGE="https://github.com/spatialaudio/python-sounddevice/ https://pypi.org/project/sounddevice/"
SRC_URI="https://github.com/spatialaudio/python-sounddevice/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/cffi
	media-libs/portaudio"
RDEPEND="${DEPEND}"

S=${WORKDIR}/python-${P}
