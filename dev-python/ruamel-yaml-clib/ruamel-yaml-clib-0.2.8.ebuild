# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

MY_PN="${PN//-/.}"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="C-based reader/scanner and emitter for dev-python/ruamel-yaml"
HOMEPAGE="https://pypi.org/project/ruamel.yaml.clib/ https://sourceforge.net/p/ruamel-yaml-clib"
SRC_URI="mirror://sourceforge/ruamel-dl-tagged-releases/${MY_P}.tar.xz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~ia64 ~loong ~ppc ~ppc64 ~riscv ~sparc ~x86"

BDEPEND="
	dev-python/cython[${PYTHON_USEDEP}]
"

S=${WORKDIR}/ruamel_yaml_clib

src_unpack() {
	default
	mv "${MY_P}" ruamel_yaml_clib || die
}

src_configure() {
	cython -f -3 _ruamel_yaml.pyx || die
}
