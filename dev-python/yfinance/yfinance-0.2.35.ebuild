# Copyright 2023-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Download market data from Yahoo! Finance's API"
HOMEPAGE="https://aroussi.com/post/python-yahoo-finance"
SRC_URI="https://github.com/ranaroussi/yfinance/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-python/pandas-1.3.0
	>=dev-python/numpy-1.16.5
	>=dev-python/requests-2.31
	>=dev-python/lxml-4.9.1
	>=dev-python/appdirs-1.4.4
	>=dev-python/pytz-2022.5
	>=dev-python/frozendict-2.3.4
	>=dev-python/beautifulsoup4-4.11.1
	>=dev-python/html5lib-1.1
	>=dev-python/peewee-3.16.2
"
