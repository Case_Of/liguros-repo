# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake xdg

DESCRIPTION="A cross-platform, terminal-based audio engine, library, player and server"
HOMEPAGE="https://musikcube.com"
SRC_URI="https://github.com/clangen/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"
IUSE="alsa libressl pulseaudio"

DEPEND="
	media-libs/libogg
	!libressl? ( dev-libs/openssl )
	libressl? ( dev-libs/libressl )
	media-libs/libvorbis
	media-video/ffmpeg
	net-libs/libmicrohttpd
	media-sound/lame
	dev-libs/libev
	media-libs/taglib
	sys-libs/ncurses
	sys-libs/zlib
	net-misc/curl
	media-libs/libopenmpt
	media-sound/mpg123
	dev-cpp/asio
	media-sound/sndio
	media-libs/game-music-emu
	alsa? ( media-libs/alsa-lib )
	pulseaudio? ( || ( media-sound/pulseaudio
					media-sound/apulse
	) )
"

BDEPEND="
	dev-build/cmake
	dev-build/ninja
	dev-libs/boost
	dev-util/patchelf
"

LICENSE="MIT"
SLOT="0"

CMAKE_IN_SOURCE_BUILD=True

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_BUILD_TYPE=Release
		-DENABLE_BUNDLED_TAGLIB=false
		-DNCURSES_DISABLE_LIB_SUFFIXES=true
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}

