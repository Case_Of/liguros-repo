# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Clock using lib ncurses"
HOMEPAGE="https://github.com/xorg62/tty-clock"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="BSD"
SLOT="0"

DEPEND="sys-libs/ncurses"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

DOCS=(
	README
)

src_install() {
	emake install \
		DESTDIR="${D}" \
		PREFIX="${EPREFIX}/usr"

	einstalldocs
}
