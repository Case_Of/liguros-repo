# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Mailfilter is a utility to get rid of unwanted spam mails"
HOMEPAGE="https://sourceforge.net/projects/mailfilter/"
SRC_URI="https://github.com/nondeterministic/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ppc sparc x86"
IUSE="libressl +ssl"

DEPEND="sys-devel/flex
	ssl? (
		libressl? ( dev-libs/libressl:0= )
		!libressl? ( dev-libs/openssl:0= )
)"

RDEPEND=""

src_prepare() {
	${S}/autogen.sh
	default
}

src_configure() {
	econf $(use_with ssl openssl)
}

src_install() {
	default
	dodoc INSTALL doc/FAQ "${FILESDIR}"/rcfile.example{1,2}
}
