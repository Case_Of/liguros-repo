# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Tool to convert several subtitle formats into subviewer srt"
HOMEPAGE="https://github.com/robelix/sub2srt"
SRC_URI="https://github.com/robelix/sub2srtdownload/${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="amd64 ppc x86"
IUSE=""

RDEPEND="dev-lang/perl"

src_install() {
	dobin sub2srt
	dodoc README
}
