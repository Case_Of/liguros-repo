# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit perl-module bash-completion-r1

DESCRIPTION="Anonymous reporting tool for LiGurOS Linux"
HOMEPAGE="https://github.com/haxmeister/liguros-reporter"
GIT_COMMIT="b6caba46a8f0b1169ed2e375becfcf9cf1234163"
SRC_URI="https://github.com/haxmeister/liguros-reporter/archive/${GIT_COMMIT}.tar.gz -> ${P}.tar.gz"
KEYWORDS="*"
S="${WORKDIR}/liguros-reporter-${GIT_COMMIT}"

LICENSE="MIT"
SLOT="0"
IUSE="${IUSE} bash-completion zsh-completion"

RDEPEND="
	dev-perl/JSON
	sys-apps/pciutils
"

DIST_TEST="do parallel"

src_compile() {
	pod2man liguros-report > liguros-report.1 || die "pod2man failed"
	pod2man lib/Liguros/Report.pm > liguros-report.3 || die "pod2man failed"
}

src_install() {
	insinto /etc
	doins "${FILESDIR}/liguros-report.conf"
	dodoc README.md
	doman liguros-report.1 liguros-report.3

	# Install bash completion files
	if use bash-completion; then
		newbashcomp share/bash-completion/liguros-report.bash liguros-report
	fi

	# Install zsh completion files
	if use zsh-completion; then
		insinto /usr/share/zsh/site-functions
		doins share/zsh-completion/_liguros-report
	fi

	perl-module_src_install
}

pkg_postinst() {
	elog LiGurOS Reporter - Stable release
	elog "The LiGurOS Reporter comes with a default config file that can be found in /etc/liguros-report.conf"
	elog "You can review what information gets submitted and generate/update your config file"
	elog "using the tool directly by issuing:"
	echo
	elog "liguros-report -u"
	echo
	elog "It is recommended to run --update-config|-u after any major release."
	elog "You can setup a cron job to submit your information on regular basis."
	elog "The data collected are submitted with a timestamp, so changes can be followed in time (like kits used, profile usage ...)."
	elog "Here is a sample cron job definition to put into your crontab:"
	echo
	elog "0 * * * * /usr/bin/liguros-report -s"
	echo
	elog "This would send data every hour to the database."
	echo
}
