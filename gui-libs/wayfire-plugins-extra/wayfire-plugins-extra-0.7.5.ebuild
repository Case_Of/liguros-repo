# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

DESCRIPTION="extra plugins for wayfire"
HOMEPAGE="https://github.com/WayfireWM/wayfire-plugins-extra"
SRC_URI="https://github.com/WayfireWM/wayfire-plugins-extra/releases/download/v${PV}/${P}.tar.xz"
KEYWORDS="amd64 ~arm64 ~x86"

LICENSE="MIT"
SLOT="0"

DEPEND="
	dev-cpp/glibmm:2
	>=gui-libs/wlroots-0.16.0:=
	>=gui-wm/wayfire-0.7.5
	x11-libs/cairo
"
RDEPEND="${DEPEND}"
BDEPEND="
	dev-libs/wayland-protocols
	virtual/pkgconfig
"
