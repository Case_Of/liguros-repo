# Copyright 2022-2024 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

SRC_URI="https://gitlab.freedesktop.org/libdecor/libdecor/-/archive/${PV}/${P}.tar.gz"
KEYWORDS="~amd64"

DESCRIPTION="A client-side decorations library for Wayland clients"
HOMEPAGE="https://gitlab.freedesktop.org/libdecor/libdecor"
LICENSE="MIT"
SLOT="0"
IUSE="+dbus"

DEPEND="
	>=dev-libs/wayland-1.18
	>=dev-libs/wayland-protocols-1.15
	gui-libs/egl-wayland
	dbus? ( sys-apps/dbus )
	x11-libs/pango
	x11-libs/libxkbcommon
"
RDEPEND="${DEPEND}"
BDEPEND=">=dev-build/meson-0.47"
