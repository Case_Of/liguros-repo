# Copyright 2020-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
USE_RUBY="ruby30 ruby31 ruby32"
RUBY_FAKEGEM_TASK_TEST=""

inherit ruby-fakegem

DESCRIPTION="A cross-platform ruby library for retrieving facts from operating systems"
HOMEPAGE="https://github.com/puppetlabs/facter"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="test"
KEYWORDS="~amd64 ~arm ~hppa ~ppc ~ppc64 ~sparc ~x86"
SRC_URI="https://github.com/puppetlabs/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

RESTRICT="!test? ( test )"
IUSE="libressl test"

ruby_add_bdepend "test? ( dev-ruby/rake dev-ruby/rspec:2 dev-ruby/mocha:0.14 )"

RDEPEND="
	dev-ruby/hocon
	dev-ruby/thor
	>=dev-libs/leatherman-1.0.0:=
	!libressl? ( dev-libs/openssl:0= )
	libressl? ( dev-libs/libressl:0= )
	sys-apps/util-linux
	app-emulation/virt-what
	net-misc/curl
	dev-libs/boost:=[nls]
	>=dev-cpp/yaml-cpp-0.5.1
	!<app-admin/puppet-4.0.0"
DEPEND="${RDEPEND}"

S="${WORKDIR}/all/${P}"

all_ruby_install() {
	all_fakegem_install
}
