# Copyright 2020-2021 LiGurOs Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="LiGurOS's networking scripts."
HOMEPAGE="https://liguros.gitlab.io"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="*"

SRC_URI="https://www.gitlab.com/liguros/${PN}/-/archive/${PV}/${P}.tar.bz2"

RDEPEND="sys-apps/openrc !<=sys-apps/openrc-0.12.4-r4"

src_install() {
	exeinto /etc/init.d || die
	doexe init.d/{netif.tmpl,net.lo} || die
	cp -a netif.d ${D}/etc || die
	chown -R root:root ${D}/etc/netif.d || die
	chmod 0755 ${D}/etc/netif.d || die
	chmod -R 0644 ${D}/etc/netif.d/* || die
}
