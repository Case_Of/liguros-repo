# Copyright 2021-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

DESCRIPTION="Modern open source high performance RPC framework"
HOMEPAGE="https://www.grpc.io"
PROTOBUF_COMMIT="2dca62f7296e5b49d729f7384f975cecb38382a0"
SRC_URI="
	https://github.com/grpc/grpc/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/census-instrumentation/opencensus-proto/archive/refs/tags/v0.4.1.tar.gz
	https://github.com/protocolbuffers/protobuf/archive/${PROTOBUF_COMMIT}.tar.gz
"

LICENSE="Apache-2.0"
# format is 0/${CORE_SOVERSION//./}.${CPP_SOVERSION//./} , check top level CMakeLists.txt
SLOT="0/33.156"
KEYWORDS="~amd64 ~ppc64 ~x86"
IUSE="doc examples libressl test"

# look for submodule versions in third_party dir
RDEPEND="
	>=dev-cpp/abseil-cpp-20230125.3
	>=dev-libs/re2-0.2021.04.01:=
	!libressl? ( >=dev-libs/openssl-1.0.2:0= )
	libressl? ( dev-libs/libressl:0= )
	>=net-dns/c-ares-1.15.0:=
	>=dev-libs/protobuf-23.1
	sys-libs/zlib:=
"
#	>=dev-python/protobuf-python-4.21.3:=
#	<dev-python/protobuf-python-5.0.0

DEPEND="${RDEPEND}
	test? (
		dev-cpp/benchmark
		dev-cpp/gflags
	)
"

BDEPEND="virtual/pkgconfig"

# requires sources of many google tools
RESTRICT="test"

soversion_check() {
	local core_sover cpp_sover
	# extract quoted number. line we check looks like this: 'set(gRPC_CPP_SOVERSION    "1.37")'
	core_sover="$(grep 'set(gRPC_CORE_SOVERSION ' CMakeLists.txt  | sed '/.*\"\(.*\)\".*/ s//\1/')"
	cpp_sover="$(grep 'set(gRPC_CPP_SOVERSION ' CMakeLists.txt  | sed '/.*\"\(.*\)\".*/ s//\1/')"
	# remove dots, e.g. 1.37 -> 137
	core_sover="${core_sover//./}"
	cpp_sover="${cpp_sover//./}"
	[[ ${core_sover} -eq $(ver_cut 2 ${SLOT}) ]] || die "fix core sublot! should be ${core_sover}"
	[[ ${cpp_sover} -eq $(ver_cut 3 ${SLOT}) ]] || die "fix cpp sublot! should be ${cpp_sover}"
}

src_prepare() {
	cmake_src_prepare

	# mv third party sources into tree
	cp -r ${WORKDIR}/opencensus-proto-0.4.1/* ${S}/third_party/opencensus-proto
	rm -rf ${WORKDIR}/opencensus-proto-0.4.1
	cp -r ${WORKDIR}/protobuf-${PROTOBUF_COMMIT}/* ${S}/third_party/protobuf
	rm -rf ${WORKDIR}/protobuf-${PROTOBUF_COMMIT}

	# un-hardcode libdir
	sed -i "s@lib/pkgconfig@$(get_libdir)/pkgconfig@" CMakeLists.txt || die
	sed -i "s@/lib@/$(get_libdir)@" cmake/pkg-config-template.pc.in || die

	soversion_check
}

src_configure() {
	local mycmakeargs=(
		-DgRPC_INSTALL=ON
		-DgRPC_ABSL_PROVIDER=package
		-DgRPC_BACKWARDS_COMPATIBILITY_MODE=OFF
		-DgRPC_CARES_PROVIDER=package
		-DgRPC_INSTALL_CMAKEDIR="$(get_libdir)/cmake/${PN}"
		-DgRPC_INSTALL_LIBDIR="$(get_libdir)"
		-DgRPC_PROTOBUF_PROVIDER=package
		-DgRPC_RE2_PROVIDER=package
		-DgRPC_SSL_PROVIDER=package
		-DgRPC_ZLIB_PROVIDER=package
		-DgRPC_BUILD_TESTS=$(usex test)
		-DCMAKE_CXX_STANDARD=17
		$(usex test '-DgRPC_GFLAGS_PROVIDER=package' '')
		$(usex test '-DgRPC_BENCHMARK_PROVIDER=package' '')
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	if use examples; then
		find examples -name '.gitignore' -delete || die
		dodoc -r examples
		docompress -x /usr/share/doc/${PF}/examples
	fi

	if use doc; then
		find doc -name '.gitignore' -delete || die
		local DOCS=( AUTHORS CONCEPTS.md README.md TROUBLESHOOTING.md doc/. )
	fi

	einstalldocs
}
