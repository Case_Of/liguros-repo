# Copyright 20221 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="nss module to have an additional passwd, shadow and group file"
HOMEPAGE="https://packages.debian.org/de/sid/libnss-extrausers"
SRC_URI="
	http://deb.debian.org/debian/pool/main/libn/${PN}/${PN}_0.6-4.1.debian.tar.xz
	http://deb.debian.org/debian/pool/main/libn/${PN}/${PN}_0.6.orig.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

S=${WORKDIR}/${PN}-0.6

src_prepare() {
	for patch in $(< "${WORKDIR}"/debian/patches/series); do
		eapply "${WORKDIR}"/debian/patches/${patch}
	done
	default
}

src_compile() {
	emake -j1
}

src_install() {
	emake DESTDIR="${D}" libprefix="/usr/$(get_libdir)" install
}
