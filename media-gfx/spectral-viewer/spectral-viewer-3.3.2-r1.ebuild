# Copyright 2022-2023 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake xdg

DESCRIPTION="Multiplatform tool for visualising/manipulating spectral images and HDR images"
HOMEPAGE="https://mrf-devteam.gitlab.io/spectral-viewer/"
MRF_GIT="87631d2795ede8922d7a09c6c113d85ba2eec3b7"
SRC_URI="
	https://gitlab.com/mrf-devteam/spectral-viewer/-/archive/v${PV}/${PN}-v${PV}.tar.gz
	https://gitlab.com/mrf-devteam/mrf/-/archive/${MRF_GIT}/mrf-${MRF_GIT}.tar.gz
"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~mips ppc ppc64 ~s390 sparc x86 ~amd64-linux ~x86-linux ~ppc-macos ~x86-macos"
IUSE=""

RDEPEND="
	media-libs/tiff
	sys-libs/zlib
	dev-qt/qtcharts:5
"
DEPEND="
	${RDEPEND}
	dev-cpp/eigen
"

S=${WORKDIR}/${PN}-v${PV}

src_prepare() {
	mv ${WORKDIR}/mrf-${MRF_GIT}/* ${S}/externals/mrf
	rm -rf ${WORKDIR}/mrf-${MRF_GIT}
	# Fix faulty line
	sed -i "/^$/d" ${S}/deploy/mrf-devteam.spectralviewer.xml || die
	cmake_src_prepare
}

src_configure() {
	cmake_src_configure
}

src_install() {
	cmake_src_install
}


pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

